## mavenMultiModule001

Proyecto pruebas con multimodulos en maven. Aniadido proyecto padre (mavenMultiModule001) con un solo proyecto hijo (master), este proyecto es de tipo web.

Generacion de proyecto 'padre'

**mavenMultiModule001:**

`mvn archetype:generate`

Cambiamos el paquete (packaging) del pom.xml de jar a pom.

Generacion del proyecto hijo.

**master:**

`mvn archetype:generate -DgroupId=org.mbracero -DartifactId=master -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false`

**NO ejecutamos el siguiente comando ya que cuando se importa a eclipse da problemas**

`mvn -pl master/ eclipse:eclipse`

Directamente lo vamos a importar en eclipse (con el plugin M2Eclipse):
```
Maven Eclipse plugin installation step by step:

Open Eclipse IDE
Click Help -> Install New Software...
Click Add button at top right corner
At pop up: fill up Name as "M2Eclipse" and Location as "http://download.eclipse.org/technology/m2e/releases"
Now click OK
```



**Unbound classpath variable: �M2_REPO/junit/junit/3.8.1/junit-3.8.1jar� in project �SampleProject�**
```
I have encountered the following error the first time I�ve created a Maven project using mvn archetype:generate then  mvn clean eclipse:clean eclipse:eclipse.

The problem had appeared that the project has dependencies on libraries which are managed with Maven, which Eclipse isn�t automatically setup to handle.

In order to solve the issue, you need to run mvn install in the project�s root directory. This should download all of the required dependencies needed by the project.

Lastly, you need to go to Eclipse and configure it to make it aware of your local Maven repository.

Window -> Preferences
Java -> Build Path -> Classpath Variables -> New
name will be M2_REPO
path will be something like /home/junie/.m2/repository
Click the OK button twice
After doing all the steps above and a recompile in Eclipse, all the related errors in the project should go away.
```


Para correr el servidor:

`mvn -pl master/ jetty:run`


Ejecutar prueba del servlet (en un navegador)

`http://localhost:9091/master/customservleturl`






http://books.sonatype.com/mvnex-book/reference/index.html


